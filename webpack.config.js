var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');


// Conditionally return a list of plugins to use based on the current environment.
// Repeat this pattern for any other config key (ie: loaders, etc).
function getPlugins(isProd) {
    var plugins = [
        new ExtractTextPlugin({ // define where to save the file
            filename: '[name].css',
            allChunks: true,
        }),
    ];

    // Conditionally add plugins for Production builds.
    if (isProd) {
        plugins.push(new webpack.optimize.UglifyJsPlugin({}));
        plugins.push(new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('production')
        }));

    // Conditionally add plugins for Development
    } else {
        plugins.push(new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('development')
        }));
        //plugins.push(new BundleAnalyzerPlugin());
    }

    return plugins;
}


module.exports = function (env) {
    var prod = false;
    if (typeof env !== "undefined" && env === "production") {
        prod = true;
    }

    return {
        target: 'web',
        devServer: {
            hot: true,
            port: 8080,
            contentBase: "dist/",
            historyApiFallback: true
        },
        entry: {
            main: ['./src/main.js', './scss/main.scss']
        },
        externals: {
            jquery: "jQuery"
        },
        output: {
            filename: '[name].js',
            path: path.resolve(__dirname, 'dist'),
            sourceMapFilename: '[file].map'
        },
        module: {
            loaders: [
                {
                    test: /\.js$/,
                    //exclude: /node_modules/,
                    loader: 'babel-loader?sourceMap',
                    query: {presets: ["env", 'react']}
                },
                {
                    test: /\.json$/,
                    exclude: /node_modules/,
                    loader: 'json-loader',
                },
                {
                    test: /\.scss$/,
                    loader: ExtractTextPlugin.extract(["css-loader?sourceMap", "postcss-loader?sourceMap", "sass-loader?sourceMap"])
                },
                {
                    test: /\.css$/,
                    loader: ExtractTextPlugin.extract(["css-loader?sourceMap", "postcss-loader?sourceMap"])
                }
            ]
        },
        plugins: getPlugins(prod),
    };
};