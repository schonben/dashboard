import backendHandler from "./backendHandler";
import store from "../store";
import * as actionCreators from "../actionCreators";

export default class endpoints extends backendHandler {
    getScreen () {
        this.callGet("screen",{},(data) => {
            store.dispatch(actionCreators.populateWidgets(data));
        },(error) => {
            if (error.status === 404) {
                // No screen found, try to register it.
                this.callPost("screen");
            }
            console.debug(error);
        });
    }
}