import endpoints from "./endpoints";

const settings = require('../../settings.json');

const backend = new endpoints(settings.api);

export default backend;