import axios from "axios";
import {getDashId} from "../localStorage";

export default class backendHandler {
    constructor(settings, timeout = 15000) {
        this.settings = settings;
        this.newInstance(timeout);
    }
    apiBase() {
        return this.settings.uri;
    }
    apiEndPoint(endPoint) {
        return this.settings.path + '/' + endPoint
    }
    newInstance(timeout=15000) {
        this.instance = axios.create({
            baseURL: this.apiBase(),
            timeout: timeout,
        });
    }

    // Possible Axois configs can be added here.
    static getConfig(params={}) {
        return {
            headers: {'DashId': getDashId()},
            params
        };
    }

    // Method that gets called for all responses from Axios calls.
    static responseHandler(response) {
        // Add general response handling here.
    }

    // Method that gets called for all errors from Axios calls.
    static errorHandler(error, method="", endpoint="") {
        if (error.response && error.response.status !== 404) {
            console.error(error.response);
            let message = "";
            if (error.response.data && error.response.data.error_message) {
                message = "Error."+error.response.data.error_message;
                // Show the message to the user
            }
        } else {
            console.debug("404 not found");
            // 404 or undefined error
        }
        // Turn off progress indicator here
    }

    // Post call wrapper.
    callPost(endpoint, data={}, successHandler=null, failureHandler=null, update=undefined) {
        this.instance
            .post(
                this.apiEndPoint(endpoint),data,backendHandler.getConfig())
            .then((response) => {
                if (response.status >= 200 && response.status < 300) {
                    if (typeof successHandler === "function") {
                        // Call success handler
                        successHandler(response.data);
                    }
                }
                backendInstance.responseHandler(response);
            })
            .catch((error) => {
                backendHandler.errorHandler(error,"post",endpoint);
                if (typeof failureHandler === "function") {
                    // Call failure handler
                    failureHandler(error.response);
                }
            });
    }
    // Get call wrapper.
    callGet(endpoint, params={}, successHandler=null, failureHandler=null) {
        this.instance
            .get(
                this.apiEndPoint(endpoint),backendHandler.getConfig(params))
            .then((response) => {
                if (response.status >= 200 && response.status < 300 && typeof successHandler === "function") {
                    // Call success handler
                    successHandler(response.data);
                }
                backendHandler.responseHandler(response);
            })
            .catch((error) => {
                backendHandler.errorHandler(error,"get",endpoint);
                if (typeof failureHandler === "function") {
                    // Call failure handler
                    failureHandler(error.response);
                }
            });
    }
    // Delete call wrapper.
    callDelete(endpoint, successHandler=null, failureHandler=null) {
        this.instance
            .delete(
                this.apiEndPoint(endpoint),backendHandler.getConfig())
            .then((response) => {
                if (response.status >= 200 && response.status < 300 && typeof successHandler == "function") {
                    // Call success handler
                    successHandler(response.data);
                }
                backendHandler.responseHandler(response);
            })
            .catch((error) => {
                backendHandler.errorHandler(error,"get",type,endpoint);
                if (typeof failureHandler === "function") {
                    // Call failure handler
                    failureHandler(error.response);
                }
            });
    }

}