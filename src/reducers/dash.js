import { fromJS, Map, List } from 'immutable';

const initialState = Map({dashId: "",date: Map({year:0,month:0,day:0,hour:0,minute:0,dow:0,week:0}),widgets: List([])});

export default function(state=initialState, action) {
    switch (action.type) {
        case "DASH_SET_ID":
            return state.setIn(["dashId"],action.payload);
            break;
        case "DASH_SET_DATE":
            return state.setIn(["date"],action.payload);
            break;
        case "DASH_POPULATE_WIDGETS":
            return state.setIn(["widgets"],action.payload);
            break;
    }
    return state;
}