import { combineReducers } from "redux-immutable";

import routing from "./routing";
import dash from "./dash";

const rootReducer = combineReducers({
    dash,
    routing
});

export default rootReducer;