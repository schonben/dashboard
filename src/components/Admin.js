import React from "react";
import Component from "./Component";

export default class App extends Component {
    render () {
        return (
            <div className="app-container">
                <div className="top-bar">
                    {/* <img src="/assets/icon-menu.svg" width="32" height="32" /> */}
                </div>
                <div className="content">
                    {React.cloneElement(this.props.children,this.props)}
                </div>
            </div>
        );
    }
}