import React from "react";
import Component from "./Component";
import Clock from "./blocks/Clock";

export default class Dashboard extends Component {
    render () {
        const dash = this.props.store.get("dash").toJS();
        return (
            <div className={"dashboard"}>
                <div className={"title"}>
                    <img src={"https://holvi.jco.fi/jco-logo-white_1000px.png"} alt={"JCO"}/>
                </div>
                <Clock date={dash.date}/>
                <div className={"dash-container"}>
                    {dash.widgets.map((widget,index) => {
                        return (
                            <div key={"widget"+index} className={"widget"}>
                                <h2>{widget}</h2>
                            </div>
                        );

                    })}
                </div>
                <div className={"dash-id"}>
                    {this.props.store.get("dash").get("dashId")}
                </div>
            </div>
        );
    }
}