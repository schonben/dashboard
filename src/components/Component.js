import React from "react";

export default class Component extends React.Component {
    componentWillMount() {
        this.update(this.props);
    }
    componentWillReceiveProps(props) {
        this.update(props);
    }
    update (props) {
        // Override this for initialization that always load.
    }
}