import React from "react";
import Component from "../Component";

export default class Clock extends Component {
    showMonth (m) {
        const month = ["January","February","March","April","May","June","July","August","September","October","November","December"];
        return month[m];
    }
    showDOW (d) {
        const dow = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
        return dow[d];
    }

    render () {
        const date = this.props.date;
        return (
            <div id={"clock"}>
                <span>
                    {this.showDOW(date.dow)} {date.day} {this.showMonth(date.month)}
                </span>
                
                <time>
                    {('0'+date.hour).slice(-2)}:{('0'+date.minute).slice(-2)}
                </time>
            </div>
        );
    }
}