import React from 'react';
import { render } from 'react-dom';
import router from './router';
import {getDashId} from "./localStorage";
import store from "./store";
import * as actionCreators from './actionCreators';
import {isUUID} from "./utils";
import backend from './backend';

init();

render(
    router,
    document.getElementById('app')
);

function init () {
    console.log(process.env.NODE_ENV);

    backend.getScreen();
    if (!isUUID(store.getState().get("dash").get("dashId"))) {
        const dashId = getDashId();
        store.dispatch(actionCreators.setDashId(dashId));
    }

    setInterval(intervalCheck,1000);
}

function intervalCheck() {

    const d = new Date();
    const dat = store.getState().get("dash").get("date");
    if (dat === undefined ||
        d.getMinutes() != dat.get("minute") ||
        d.getHours() != dat.get("hour") ||
        d.getDate() != dat.get("day")) {
        store.dispatch(actionCreators.updateDate());
    }
}