import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import { syncHistoryWithStore, routerMiddleware } from 'react-router-redux';
import { browserHistory } from "react-router";
import reducer from './reducers';
import { loadState, saveState } from './localStorage';

const router = routerMiddleware(browserHistory);
const store = createStore(
    reducer,
    composeWithDevTools(applyMiddleware(router))
);

//store.subscribe(throttle(() => {
    //saveState({
    //    dash: store.getState().get("dash").toJS()
    //});
//},1000));

/* Create enhanced history object for router */
const createSelectLocationState = () => {
    let prevRoutingState, prevRoutingStateJS;
    return (state) => {
        const routingState = state.get('routing'); // or state.routing
        if (typeof prevRoutingState === 'undefined' || prevRoutingState !== routingState) {
            prevRoutingState = routingState;
            prevRoutingStateJS = routingState.toJS();
        }
        return prevRoutingStateJS;
    };
};

export const history = syncHistoryWithStore(
    browserHistory,
    store,
    {
        selectLocationState: createSelectLocationState()
    }
);
export default store;