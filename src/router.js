import React from "react";
import { Provider, connect} from "react-redux";
import {bindActionCreators} from "redux";
import {IndexRoute, Route, Router} from "react-router";
import store, {history} from "./store";
import * as actionCreators from './actionCreators';

import Dashboard from "./components/Dashboard";
import Admin from "./components/Admin";
import Index from "./components/admin/Index";

function mapStateToProps(state) {
    return { store: state };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators(actionCreators,dispatch);
}

const router = (
    <Provider store={store}>
        <Router  onUpdate={() => window.scrollTo(0, 0)} history={history}>
            <Route path={"/admin"} component={connect(mapStateToProps,mapDispatchToProps)(Admin)}>
                <IndexRoute component={Index}/>
            </Route>
            <Route path={"/"} component={connect(mapStateToProps,mapDispatchToProps)(Dashboard)}/>
        </Router>
    </Provider>
);
export default router;