import * as Immutable from "immutable";
import uuidv4 from "uuid";

export const loadState = () => {
    try {
        const serializedState = localStorage.getItem('state');
        if (serializedState === null) {
            return undefined;
        }
        return Immutable.fromJS(JSON.parse(serializedState));
    } catch (err) {
        return undefined;
    }
};

export const saveState = (state) => {
    try {
        const jsonState = JSON.stringify(state);
        localStorage.setItem('state',jsonState);
    } catch (err) {
    }
};

export function getDashId() {
    try {
        const id = localStorage.getItem("DashId");
        if (id !== null) {
            return id;
        }
    } catch (err) {
        console.debug("No DashId stored, generating....");
    }
    const newId = uuidv4();
    setDashId(newId);
    return newId;
}

export function setDashId (id) {
    try {
        localStorage.setItem("DashId",id);
    } catch (err) {
        console.debug("Setting Id Failed");
    }
}