import { push } from 'react-router-redux'
import { fromJS, Map, List } from 'immutable';
import {getWeekNumber} from "./utils";

// Router
export function routePush(path) {
    return push(path);
}


// Dash
export function setDashId(id) {
    return {
        type: 'DASH_SET_ID',
        payload: id
    }
}
export function updateDate() {
    const d = new Date();
    return {
        type: 'DASH_SET_DATE',
        payload: Map({year:d.getFullYear(),month:d.getMonth(),day:d.getDate(),hour:d.getHours(),minute:d.getMinutes(),dow:d.getDay(),week:getWeekNumber(d)})
    }
}
export function populateWidgets(data) {
    return {
        type: 'DASH_POPULATE_WIDGETS',
        payload: data.widgets
    }
}
